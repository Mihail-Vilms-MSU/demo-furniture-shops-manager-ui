export class Shop {
  shopId: number;
  name: string;
  city: string;
  state: string;
  address: string;
  phone: string;
  createdAt: Date; // Date in java
  updatedAt: Date; // Date in java
}
