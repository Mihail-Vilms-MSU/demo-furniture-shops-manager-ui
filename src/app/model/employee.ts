import {Shop} from './shop';

export class Employee {
  employeeId: number;
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
  role: string;

  shop: Shop;
  shopUrl: string;
}
